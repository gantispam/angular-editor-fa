# AngularEditorFa

A simple native WYSIWYG editor for Angular 6+, 7+, 8+. Rich Text editor component for Angular.

This is a Fork from <https://www.npmjs.com/package/@kolkov/angular-editor> that used FontAwesome dependencies (without CDN).

# Getting Started
## Installation
* Prerequis
  * Install FontAwesome : <https://www.npmjs.com/package/@fortawesome/angular-fontawesome>

Install via [npm][npm] package manager 

```bash
npm install @gsig/angular-editor-fa --save
```


### Usage

Import `angular-editor-fa` module

```typescript
import { HttpClientModule} from '@angular/common/http';
import { AngularEditorFaModule } from '@gsig/angular-editor-fa';

@NgModule({
  imports: [ HttpClientModule, AngularEditorFaModule ]
})
```

Then in HTML

```html
<angular-editor-fa [placeholder]="'Enter text here...'" [(ngModel)]="htmlContent"></angular-editor-fa>
```

or for usage with reactive forms

```html
<angular-editor-fa formControlName="htmlContent" [config]="editorConfig"></angular-editor-fa>
```

if you using more than one editor on same page set `id` property

```html
<angular-editor-fa id="editor1" formControlName="htmlContent1" [config]="editorConfig"></angular-editor-fa>
<angular-editor-fa id="editor2" formControlName="htmlContent2" [config]="editorConfig"></angular-editor-fa>

```

where

```typescript
import { AngularEditorFaConfig } from '@gsig/angular-editor-fa';

...

editorConfig: IAngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
};
```

For `ngModel` to work, you must import `FormsModule` from `@angular/forms`, or for `formControlName`, you must import `ReactiveFormsModule` from `@angular/forms`


## Documentation

The documentation for the AngularEditor is hosted at our website [AngularEditor](https://angular-editor.kolkov.ru/)


## Creators

**Andrey Kolkov**

* <https://github.com/kolkov>

## Fork
**GSIG**

* <https://framagit.org/gantispam/>
