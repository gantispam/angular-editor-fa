# Whats is it?

- This is a local fork from https://www.npmjs.com/package/@kolkov/angular-editor

# Why?

- AngularEditor is opensource and MIT licence.
- So, AngularEditor use forbidden CDN for fontawesome.

# Note

- At this time all Wysiwyg is broken by a major update from zone.js (0.9+).
- This is an know issue on angular repository and ckeditor repository.
- For exemple this issue give unresovable error : TypeError: Cannot read property 'data-ck-expando' of undefined.
- So, we do a local fork here for use angular-editor without CDN link (sorry).
