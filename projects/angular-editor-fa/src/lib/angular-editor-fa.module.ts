import { NgModule } from '@angular/core';
import { AngularEditorFaComponent } from './angular-editor-fa.component';
import { AngularEditorToolbarComponent } from './angular-editor-toolbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AeSelectComponent } from './ae-select/ae-select.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, FontAwesomeModule],
  declarations: [AngularEditorFaComponent, AngularEditorToolbarComponent, AeSelectComponent],
  exports: [AngularEditorFaComponent, AngularEditorToolbarComponent]
})
export class AngularEditorFaModule {}
