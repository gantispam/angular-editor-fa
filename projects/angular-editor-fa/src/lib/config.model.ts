export interface ICustomClass {
  name: string;
  class: string;
  tag?: string;
}

export interface IFont {
  name: string;
  class: string;
}

export interface IAngularEditorConfig {
  editable?: boolean;
  spellcheck?: boolean;
  height?: 'auto' | string;
  minHeight?: '0' | string;
  maxHeight?: 'auto' | string;
  width?: 'auto' | string;
  minWidth?: '0' | string;
  translate?: 'yes' | 'now' | string;
  enableToolbar?: boolean;
  showToolbar?: boolean;
  placeholder?: string;
  defaultParagraphSeparator?: string;
  defaultFontName?: string;
  defaultFontSize?: '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | string;
  uploadUrl?: string;
  fonts?: IFont[];
  customClasses?: ICustomClass[];
  sanitize?: boolean;
  toolbarPosition?: string;
  outline?: boolean;
}
