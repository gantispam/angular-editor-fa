/*
 * Public API Surface of angular-editor-fa
 */

export * from './lib/angular-editor-fa.service';
export * from './lib/angular-editor-fa.component';
export * from './lib/angular-editor-fa.module';
export * from './lib/config.model';
